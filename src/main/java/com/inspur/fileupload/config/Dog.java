package com.inspur.fileupload.config;

import org.springframework.core.style.ToStringCreator;

public class Dog {
	
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@Override
	public String toString() {
		
		return "Dog name is"+ this.name;
	}

}
